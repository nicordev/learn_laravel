<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//
// Public routes
//

// Using callbacks
//Route::get('/products', function () {
//    return \App\Models\Product::all();
//});
//Route::post('/products', function (Request $request) {
//    $request->validate([
//        'name' => 'required',
//        'slug' => 'required',
//        'price' => 'required',
//    ]);
//
//    return \App\Models\Product::create($request->all());
//});

// Using Route::resource and ProductController
//Route::resource('products', ProductController::class);

// Using detailed ProductController methods
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::get('/products', [ProductController::class, 'index']);
Route::get('/products/{id}', [ProductController::class, 'show']);

// Custom routes
Route::get('/products/search/{name}', [ProductController::class, 'search']);

//
// Protected routes
//

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('/easter-egg', function () {
        return 'hello';
    });
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/products', [ProductController::class, 'store']);
    Route::put('/products/{id}', [ProductController::class, 'update']);
    Route::patch('/products/{id}', [ProductController::class, 'update']);
    Route::delete('/products/{id}', [ProductController::class, 'destroy']);
});
