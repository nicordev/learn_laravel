# Learn Laravel

[Installation](https://laravel.com/docs/8.x#getting-started-on-linux) :

```bash
curl -s https://laravel.build/example-app | bash
cd example-app
./vendor/bin/sail up
```

```bash
composer create-project laravel/laravel example-app
cd example-app
php artisan serve
```

