#! /bin/bash

readonly SCRIPT_NAME=$(basename $BASH_SOURCE)
readonly SCRIPT_DIRECTORY=$(dirname $BASH_SOURCE)

_askConfirmationDefaultYes() {
    echo -e "\e[1mContinue?\e[0m [YES/no] "
    read answer

    if [[ ${answer,,} =~ ^n ]]; then
        return 1
    fi

    return 0
}

setSqliteDatabase() {
    sed -i 's/DB_CONNECTION=.*/DB_CONNECTION=sqlite/' .env
    touch database/database.sqlite
}

setMysqlDatabase() {
    sed -i 's/DB_CONNECTION=.*/DB_CONNECTION=mysql/' .env
}

makeModel() {
    if [ $# -lt 1 ]; then
        echo -e "${SCRIPT_NAME} ${FUNCNAME[0]} \e[33mmodelName\e[0m"

        return 1
    fi

    php artisan make:model "$1" --migrations
    echo "add model $1 properties here:"
    ls -1 app/Models/ | grep "$1"
    echo "then run migrate"
}

migrate() {
    php artisan migrate
}

makeController() {
    if [ $# -lt 1 ]; then
        echo -e "${SCRIPT_NAME} ${FUNCNAME[0]} \e[33mcontrollerName\e[0m"

        return 1
    fi

    php artisan make:controller "$1"
    echo "update controller $1 here:"
    ls -1 app/Http/Controllers | grep "$1"
}

makeControllerApi() {
    if [ $# -lt 1 ]; then
        echo -e "${SCRIPT_NAME} ${FUNCNAME[0]} \e[33mcontrollerName\e[0m"

        return 1
    fi

    php artisan make:controller "$1" --api
    echo "update controller $1 here:"
    ls -1 app/Http/Controllers | grep "$1"
}

addSanctumAuthentication() {
    composer require laravel/sanctum:^2.9 # for laravel 8
    php artisan vendor:publish --provider="Laravel\Sanctum\SanctumServiceProvider"
    php artisan migrate
}

# Display the source code of this file
howItWorks() {
    cat $0
}

# List all functions that do not begin with an underscore _
_listAvailableFunctions() {
    cat $0 | grep -E '^[a-z]+[a-zA-Z0-9_]*\(\) \{$' | sed 's#() {$##' | sort
}

if [ $# -eq 0 ]; then
    _listAvailableFunctions
    exit
fi

"$@"
