<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jewel extends Model
{
    use HasFactory;

    protected $table = 'jewel';
    protected $fillable = [
    	'slug',
    	'type',
    	'content',
    ];
}
