<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Support\Str;
use App\Http\Requests\PostStoreRequest;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();

        return response()->json([
            'posts' => $posts,
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStoreRequest $request)
    {
        try {
            $imageName = Str::random(32) . "." . $request->image->getClientOriginalExtension();
            Post::create([
                'name' => $request->name,
                'image' => $imageName,
                'description' => $request->description
            ]);
            // Save Image in directory storage/app/public
            Storage::disk('public')->put($imageName, file_get_contents($request->image));

            return response()->json([
                'message' => "Post successfully created."
            ], 200);
        } catch (\Exception $e) {

            return response()->json([
                'message' => "Something went really wrong!"
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        if (!$post) {
            return response()->json([
                'message' => 'Post Not Found.'
            ], 404);
        }

        return response()->json([
            'post' => $post
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostStoreRequest $request, $id)
    {
        try {
            $post = Post::find($id);

            if (!$post) {

                return response()->json([
                    'message' => "Post $id not found.",
                ], 404);
            }

            $post->name = $request->name;
            $post->description = $request->description;

            if ($request->image) {
                // Public storage
                $storage = Storage::disk('public');

                // Delete stored image
                if ($storage->exists($post->image)) {
                    $storage->delete($post->image);
                }

                // Image name
                $imageName = Str::random(32) . "." . $request->image->getClientOriginalExtension();
                $post->image = $imageName;

                // Image save in public folder
                $storage->put($imageName, file_get_contents($request->image));
            }

            // Update Post
            $post->save();

            return response()->json([
                'message' => "Post successfully updated."
            ], 200);
        } catch (\Exception $e) {

            return response()->json([
                'message' => "Something went really wrong!"
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // find post
        $post = Post::find($id);

        if (!$post) {

            return response()->json([
                'message' => "Post $id not found.",
            ], 404);
        }

        // delete image file
        $storage = Storage::disk('public');

        if ($storage->exists($post->image)) {
            $storage->delete($post->image);
        }

        // delete post
        $post->delete();

        return response()->json([
            'message' => "Post $id deleted."
        ], 200);
    }
}
