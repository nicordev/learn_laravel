# Birthday

## Model

User 1---* Birthday

Birthday 1---* Action

User:
- name: string
- email: string
- password: string

Birthday:
- name: string
- date: datetime

Action:
- type: enum
- runAt: datetime
- parameters: json

## Use cases

Visitor:
- register

User:
- save Birthday
- list Birthay
- add action to Birthday
